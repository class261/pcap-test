#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include "libnet.h"

void usage() {
	printf("syntax: pcap-test <interface>\n");
	printf("sample: pcap-test wlan0\n");
}

typedef struct {
	char* dev_;
} Param;

Param param = {
	.dev_ = NULL
};

bool parse(Param* param, int argc, char* argv[]) {
	if (argc != 2) {
		usage();
		return false;
	}
	param->dev_ = argv[1];
	return true;
}

int main(int argc, char* argv[]) {
	if (!parse(&param, argc, argv))
		return -1;

	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* pcap = pcap_open_live(param.dev_, BUFSIZ, 1, 1000, errbuf);
	if (pcap == NULL) {
		fprintf(stderr, "pcap_open_live(%s) return null - %s\n", param.dev_, errbuf);
		return -1;
	}

	int cnt = 1;

	while (true) {
		struct pcap_pkthdr* header;
		const u_char* packet;
		int res = pcap_next_ex(pcap, &header, &packet);
		if (res == 0) continue;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(pcap));
			break;
		}
		printf("%u bytes captured\n", header->caplen);

		printf("%dth packet information\n\n", cnt);

		struct libnet_ethernet_hdr *EthHeader;
		EthHeader = (struct libnet_ethernet_hdr *)packet;

		printf("========== Ethernet Header ==========\n");
		printf("src mac : %02x:%02x:%02x:%02x:%02x:%02x\n", EthHeader->ether_shost[0], EthHeader->ether_shost[1], EthHeader->ether_shost[2]\
		, EthHeader->ether_shost[3], EthHeader->ether_shost[4], EthHeader->ether_shost[5]);
		printf("dst mac : %02x:%02x:%02x:%02x:%02x:%02x\n\n", EthHeader->ether_dhost[0], EthHeader->ether_dhost[1], EthHeader->ether_dhost[2], \
		EthHeader->ether_dhost[3], EthHeader->ether_dhost[4], EthHeader->ether_dhost[5]);

		if(ntohs(EthHeader->ether_type) != ETHERTYPE_IP)
			break;
			
		packet += 14;

		struct libnet_ipv4_hdr *IPHeader;
		IPHeader = (struct libnet_ipv4_hdr *)packet;

		printf("============= IP Header =============\n");
		printf("src IP : %u.%u.%u.%u\n", IPHeader->ip_src.s_addr & 0xff, (IPHeader->ip_src.s_addr >> 8) & 0xff, \
		(IPHeader->ip_src.s_addr >> 16) & 0xff, (IPHeader->ip_src.s_addr >> 24));
		printf("src IP : %u.%u.%u.%u\n\n", IPHeader->ip_dst.s_addr & 0xff, (IPHeader->ip_dst.s_addr >> 8) & 0xff, \
		(IPHeader->ip_dst.s_addr >> 16) & 0xff, (IPHeader->ip_dst.s_addr >> 24));

		packet += IPHeader->ip_hl * 4;

		struct libnet_tcp_hdr *TCPHeader;
		TCPHeader = (struct libnet_tcp_hdr *)packet;

		if(IPHeader->ip_p != 6)
			break;

		u_int16_t sport = TCPHeader->th_sport;
		u_int16_t dport = TCPHeader->th_dport;

		u_int16_t sport1 = sport >> 8 | sport << 8;
		u_int16_t dport1 = dport >> 8 | dport << 8;

		printf("============= TCP Header =============\n");
		printf("src port : %d\n", sport1);
		printf("dst port : %d\n\n", dport1);

		packet += TCPHeader->th_off * 4;


		printf("============ Packet Data ============\n");
		for (int i = 0; i < 10; i ++)
		{
			printf("%02x ", packet[i]);
		}
		printf("\n\n");

		cnt ++;
	}

	pcap_close(pcap);
}
